import Pages.MainPage;
import Pages.ResultPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;


public class SearchTest extends TestBase {
    @Test
    public void Test(){
        driver.get("https://www.google.com/");
        MainPage mainPage = new MainPage(driver);
        mainPage.searchFor("Adam Malysz");
        ResultPage resultPage = new ResultPage(driver);

        Assert.assertEquals("Adam Małysz",resultPage.VerifyResult());
    }


}
