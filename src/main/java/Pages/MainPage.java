package Pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    WebDriver driver;

    @FindBy(id = "lst-ib")
    WebElement searchInput;
    @FindBy(css = "#lga [alt]")
    WebElement googleLogo;
//    @FindBy(css = "input[type='submit']:nth-of-type(1)")
    @FindBy(xpath = "/html//form[@id='tsf']//input[@name='btnK']")
    WebElement standardSearchButton;
    @FindBy(css = "input[type='submit']:nth-of-type(2)")
    WebElement luckySearchButton;


    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void searchFor(String textToSearch) {
        searchInput.sendKeys(textToSearch);
        googleLogo.click();
        standardSearchButton.click();
    }
}
